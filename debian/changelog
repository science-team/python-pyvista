python-pyvista (0.44.1-11) unstable; urgency=medium

  * Skip tests/plotting/test_plotting.py::test_remove_vertices_actor when testing
    on s390x due to segmentation fault on a previous test run.

 -- Francesco Ballarin <francesco.ballarin@unicatt.it>  Thu, 13 Feb 2025 12:35:26 +0000

python-pyvista (0.44.1-10) unstable; urgency=medium

  * python3-pyvista Depends: python3-imageio, python3-meshio
    Drop Depends: ${shlibs:Depends} (package is arch-independent)
  * python3-pyvista Recommends: python3-colorcet
    (cmocean is not packaged for Debian)
  * debian/py3dist-overrides provides vtk package dependency:
      vtk python3-vtk9 | python3-paraview
    Correct behaviour is only guaranteed with vtk9. The paraview
    alternative is offered as a convenience, not all functionality
    will be available.
  * debian patch paraview_skip_tests.patch skips or xfails tests which
    fail when paraview provides vtkmodules (charts, and other tests)
  * fix logic in test_meshio_i386_no_network.patch
    (don't xfail on non-i386 systems)
  * debian/tests: skip all test_rotate_should_match_vtk_rotation
    on i386 ([x], [y] as well as [z])
  * debian patch skip_trimesh_tests.patch skips trimesh tests.
    Drop python3-trimesh from debian/tests Depends. The trimesh
    package is not yet stable, and not available in testing.

 -- Drew Parsons <dparsons@debian.org>  Wed, 27 Nov 2024 19:23:04 +0100

python-pyvista (0.44.1-9) unstable; urgency=medium

  * debian patch floating_point_tests_PR6806.patch applies upstream
    PR#6806 to fix floating point tests in test_axes_assembly.py
  * test_meshio_i386_no_network.patch: add more i386 xfails to
    openfoam tests

 -- Drew Parsons <dparsons@debian.org>  Mon, 18 Nov 2024 01:41:59 +0100

python-pyvista (0.44.1-8) unstable; urgency=medium

  * test_meshio_i386_no_network.patch: add more xfail marks
  * debian/tests/run-unit-tests: skip failing tests test_plot_logo and
    test_rotate_should_match_vtk_rotation[z] on i386
  * debian/tests/run-unit-tests-network: disable test_download_files
    on i386 (since otherwise already enabled by test_download)

 -- Drew Parsons <dparsons@debian.org>  Sun, 17 Nov 2024 12:52:21 +0100

python-pyvista (0.44.1-7) unstable; urgency=medium

  * update test_meshio_i386_no_network.patch to give test parameters
    a token value (None) when HTTPError is caught. Only use beam
    example if it was set (not None).

 -- Drew Parsons <dparsons@debian.org>  Sun, 17 Nov 2024 01:30:58 +0100

python-pyvista (0.44.1-6) unstable; urgency=medium

  * debian patch test_meshio_i386_no_network.patch: reinstate test for
    HTTPError in test_meshio.py. Triggered by test collection on i386
    before test skips are activated.

 -- Drew Parsons <dparsons@debian.org>  Sat, 16 Nov 2024 01:24:47 +0100

python-pyvista (0.44.1-5) unstable; urgency=medium

  * rename debian patch test_meshio_no_network.patch as
    test_meshio_i386_no_network.patch. Mark tests which fail to
    download on ix86 (i386/i686 etc) (HTTP request refused) as xfail
    instead of testing for HTTPError.
  * s390x: skip all test_download_gltf* (segfaults),
    test_dataset_loader_from_nested_multiblock and
    test_meshio[mesh_in{0,2}]. Bad data reads, bigendian issues?

 -- Drew Parsons <dparsons@debian.org>  Thu, 14 Nov 2024 18:30:56 +0100

python-pyvista (0.44.1-4) unstable; urgency=medium

  * s390x skip of test_download_gltf_damaged_helmet belongs in network
    tests
  * update debian patch test_meshio_no_network.patch to test for
    HTTPError not ValueError
  * debian/tests: restrict trimesh dependency to arches where it is
    available [amd64 arm64 mips64el ppc64el riscv64]

 -- Drew Parsons <dparsons@debian.org>  Wed, 13 Nov 2024 12:22:35 +0100

python-pyvista (0.44.1-3) unstable; urgency=medium

  * debian/tests: fix launch of network tests
  * debian patch test_meshio_no_network.patch skips tests in
    test_meshio.py if example files are not accessible
    (e.g. i386 HTTP requests are refused)
  * skip test_import_gltf on s390x
  * skip test_multiprocessing (tests/core/test_dataset.py)
    Flakey test, often segfaults.
  * debian/tests Depends: python3-trimesh
    python3-pyvista Suggests: python3-trimesh
    (used by core/utilities/helpers.py if TYPE_CHECKING is active)

 -- Drew Parsons <dparsons@debian.org>  Tue, 12 Nov 2024 13:29:45 +0100

python-pyvista (0.44.1-2) unstable; urgency=medium

  * set python3-pyvista to Architecture: all
  * run network tests in separate debian/tests/run-unit-tests-network
    The http request to access example data files is refused for i386
    so enable downloads only for other arches.
  * skip failing tests
    - riscv64: test_axis_scale times out
    - s390x: test_download_gltf_damaged_helmet segfaults

 -- Drew Parsons <dparsons@debian.org>  Tue, 12 Nov 2024 01:39:12 +0100

python-pyvista (0.44.1-1) unstable; urgency=medium

  * New upstream release
  * debian patch no_trame_vtk.patch disables use of
    pyvista.ext.viewer_directive in tinypages tests.
    It needs trame_vtk, which is not packaged for Debian.
    Set default generated filetype to png not vtksz.

 -- Drew Parsons <dparsons@debian.org>  Sun, 10 Nov 2024 15:11:27 +0100

python-pyvista (0.43.10-5) unstable; urgency=medium

  * Skip more tests that have failed on debci while testing 0.43.10-4

 -- Francesco Ballarin <francesco.ballarin@unicatt.it>  Sat, 29 Jun 2024 06:10:04 +0000

python-pyvista (0.43.10-4) unstable; urgency=medium

  * Print pyvista report before starting autopkgtest session, since it may be
    useful to have those info when reporting issues upstream.
  * autopkgtest randomly segfaults when running pytest on debci. Take the
    following measures to investigate the segmentation faults:
    - increase pytest verbosity with -vv, so that test names are printed
    - randomize test order: this should help us determine if segfaults are
      happening after a specific test, or after a specific amount of tests have
      been run
  * Skip test_compute_derivatives on autopkgtest, since I have seen it segfault
    at least once on my laptop
  * Skip test_utilities_namespace on autopkgtest, since it does not emit the
    expected warning. This may be a side effect of test randomization, but
    investigating segmentation faults that affect the possibility of running the
    testsuite at all must take priority over checking that a handful of warnings
    are correctly emitted
  * Skip test_contour_labeled, test_contour_labeled_with_smoothing and
    test_contour_labeled_with_boundary_output_style due to max value tested in
    the assert being different from the expected one

 -- Francesco Ballarin <francesco.ballarin@unicatt.it>  Tue, 25 Jun 2024 12:53:50 +0000

python-pyvista (0.43.10-3) unstable; urgency=medium

  * Require xauth when running autopkgtest, because it is internally called by
    xvfb-run

 -- Francesco Ballarin <francesco.ballarin@unicatt.it>  Sun, 23 Jun 2024 12:20:14 +0000

python-pyvista (0.43.10-2) unstable; urgency=medium

  * Let xvfb-run handle starting and stopping the virtual X frame buffer while
    running tests in autokpgtest

 -- Francesco Ballarin <francesco.ballarin@unicatt.it>  Sun, 23 Jun 2024 05:31:23 +0000

python-pyvista (0.43.10-1) unstable; urgency=medium

  * New upstream release
  * Drop a sentence from the package description, because it refers to
    information that is not actually available in the description itself.
  * Standards-Version: 4.7.0 (routine-update)
  * Add missing build-dependency on python3-setuptools.
  * Refresh autopkgtest skips due to new tests in 0.43.10
  * Allow autopkgtest to use internet, because some data files used in tests are
    downloaded from the internet on the fly
  * In autopkgtest, make xvfb immune to hangups by running it with nohup,
    otherwise pytest may segfault as soon as xvfb closes

 -- Francesco Ballarin <francesco.ballarin@unicatt.it>  Sat, 22 Jun 2024 16:45:16 +0000

python-pyvista (0.43.2-1) unstable; urgency=medium

  * Initial release (Closes: #1001105)

 -- Andreas Tille <tille@debian.org>  Sat, 27 Jan 2024 22:51:10 +0100
